<?php
namespace PaymentBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('count_profile', ChoiceType::class, array(
            'label'    => 'Total number of establishment',
            'choices'  => $this->countProfile(),
        ));
        $builder->add('advertising_and_jingles', CheckboxType::class, array(
            'label'    => 'Advertising and jingles',
            'required' => false,
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PaymentBundle\Entity\Order'
        ));
    }

    function getName()
    {
        return 'order';
    }

    function countProfile()
    {
        $count_profile = [];
        for ($i = 1; $i<=256; $i++){
            $count_profile[$i] = $i;
        }
        return $count_profile;
    }

}