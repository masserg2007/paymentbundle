<?php
namespace PaymentBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;



class SendMailUserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('payment:send_mail_user')

            // the short description shown while running "php bin/console list"
            ->setDescription('Send email reminder of payment')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command sends reminders email of payment. 
To use add a crontab: 
0 0 * * * /usr/bin/php /var/www/adelagio/bin/console payment:send_mail_user')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $orders = $em->getRepository('PaymentBundle:Order')->findAll();
        $date = new \DateTime("now");
        $day_now = $date->format('d');
        $day_count = 15;
        $count_mount = cal_days_in_month(CAL_GREGORIAN, $date->format('m'), $date->format('Y'));
        $output->writeln([
            'Send email reminder of payment',
            '==============================',
            '',
        ]);
        foreach ($orders as $order) {
            $state = $order->getState();
            if ((($count_mount - $day_now) <= $day_count) && ($state == true)) {
                $subscribe = $order->getSubscribe();
                if ($subscribe == true) {
                    $user = $em->getRepository('UserBundle:Users')->find($order->getUserId());
                    $e_to = $user->getEmail();
                    $e_from = $this->getContainer()->getParameter('mailer_user');
                    $email_to = $e_to;
                    $email_from = $e_from;
                    $message = \Swift_Message::newInstance()
                        ->setSubject('Adelagio')
                        ->setFrom($email_from)
                        ->setTo($email_to)
                        ->setBody('До кінця місяця залишелось менше 15 днів оплата здійсниться автоматично.');
                    $this->getContainer()->get('mailer')->send($message);

                    $output->writeln('Email: send');

                } elseif ($subscribe == false) {
                    $user = $em->getRepository('UserBundle:Users')->find($order->getUserId());
                    $e_to = $user->getEmail();
                    $e_from = $this->getContainer()->getParameter('mailer_user');
                    $email_to = $e_to;
                    $email_from = $e_from;
                    $message = \Swift_Message::newInstance()
                        ->setSubject('Adelagio')
                        ->setFrom($email_from)
                        ->setTo($email_to)
                        ->setBody('До кінця місяця залишелось менше 15 днів оплатіть послуги.');
                    $this->getContainer()->get('mailer')->send($message);

                    $output->writeln('Email: send');
                }

            }
        }
    }
}