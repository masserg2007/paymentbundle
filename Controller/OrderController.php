<?php

namespace PaymentBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use PaymentBundle\Libs\LiqPay;

use PaymentBundle\Entity\Order;
use PaymentBundle\Form\Type\OrderType;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class OrderController extends Controller
{

    public function view_allAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')){
            $user_id = $this->get('security.token_storage')->getToken()->getUser()->getId();
        } else { return $this->redirect($this->generateUrl('fos_user_security_login')); }
        $user = $this->getDoctrine()->getRepository('UserBundle:Users')->find($user_id);
        $orders = $user->getAccountOrders();

        return $this->render('PaymentBundle:Order:view_all.html.twig', array(
            'orders' => $orders
        ));
    }

    public function viewAction(Request $request)
    {
        $orders = $this->getDoctrine()->getRepository('PaymentBundle:Order')->findAll();

        return $this->render('PaymentBundle:Order:view.html.twig', array(
            'orders' => $orders
        ));
    }

    public function createAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')){
            $user_id = $this->get('security.token_storage')->getToken()->getUser()->getId();
        } else { return $this->redirect($this->generateUrl('fos_user_security_login')); }
        $user = $this->getDoctrine()->getRepository('UserBundle:Users')->find($user_id);
        $value_price = 1;//$price->getPrice();

        $count = rand(1,255);
        do {
            $now = date("YmdHis");
            $random = random_bytes(16);
            $randomString = bin2hex($random.$now);
            $count++;
        }
        while($randomString && $count < 50);
        $order_id = $randomString;
        $j = 1;
        $form = $this->createForm(OrderType::class, new Order());
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST')
        {
            if ($form->isValid()) {
                $data = $form->getData();
                if($data->getAdvertisingAndJingles() != false){$j = 1.5;}
                $count_profile = $data->getCountProfile();
                $action = 'subscribe';
                $data->setCountProfile($count_profile + 2);
                $number_login = $count_profile;
                $sum = (5 + $number_login * 7)*$j;
                $data->setSumm($sum);
                $data->setCreation(new \DateTime("now"));
                $data->setUsers($user);
                $data->setState(false);
                $data->setOrderId($order_id);
                $data->setSubscribe(0);

                $em = $this->getDoctrine()->getManager();
                $em->persist($data);
                $em->flush();
                return $this->redirect($this->generateUrl('user_order_create', array(
                    'action'   => $action,
                    'sum'      => $sum,
                    'order_id' => $order_id
                )));
            }
        }
        return $this->render('PaymentBundle:Order:create.html.twig', array(
            'form' => $form->createView(),
            'value_price'=> $value_price
        ));

    }

    public function payAction($action, $sum, $order_id, Request $request)
    {
        $public_key = $this->getParameter('public_key');
        $private_key = $this->getParameter('private_key');
        $result_url = $this->generateUrl('user_order_state',['order_id'=>$order_id], UrlGeneratorInterface::ABSOLUTE_URL);
        $server_url = $this->generateUrl('user_order_state',['order_id'=>$order_id],UrlGeneratorInterface::ABSOLUTE_URL);
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')){
            $date = date('Y-m-d H:i:s');
            $liqpay = new LiqPay($public_key, $private_key);
            $html = $liqpay->cnb_form(array(
                'action'         => $action,
                'subscribe_periodicity' => 'month',
                'subscribe_date_start'  => $date,
                'amount'         => $sum,
                'currency'       => 'USD',
                'version'        => '3',
                'type'           => 'donate',
                'sandbox'        => 1,
                'description'    => 'Adelagio Service',
                'order_id'       => $order_id,
                'result_url'     => $result_url,
                'server_url'     => $server_url
            ));

            return $this->render('PaymentBundle:Liqpay:index.html.twig', array(
                'html'     => $html,
                'sum'      => $sum,
                'action'   => $action,
                'order_id' => $order_id
            ));

        } else { return $this->redirect($this->generateUrl('fos_user_security_login')); }

    }

    public function stateAction($order_id, Request $request)
    {

        $public_key = $this->getParameter('public_key');
        $private_key = $this->getParameter('private_key');
        $liqpay = new LiqPay($public_key, $private_key);
        $res = $liqpay->api("request", array(
            'action'        => 'status',
            'version'       => '3',
            'order_id'      => $order_id
        ));
        $status = $res->result;
        if ($status == "ok") {
            $em = $this->getDoctrine()->getEntityManager();
            $order = $this->getDoctrine()->getRepository('PaymentBundle:Order')->findOneBy(['order_id' => $order_id]);
            if ($order->getEnded() == null) {
                $order->setState(true);
                $now_year = date('Y');
                $now_month = date('m');
                $now_hour = date('H');
                $now_minute = date('i');

                if ($now_month < 12) {
                    $now_month++;
                } elseif ($now_month == 12) {
                    $now_month = 1;
                    $now_year++;
                }
                $end_date = "$now_year-$now_month-01 $now_hour:$now_minute:00";
                $order->setEnded(new \DateTime($end_date));
                $order->setSubscribe(1);
                $em->flush();
            }
        }
        return $this->render('PaymentBundle:Order:state.html.twig', array(
            'status' => $status
        ));
    }

}
