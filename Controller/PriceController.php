<?php

namespace PaymentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Libs\LiqPay;

use PaymentBundle\Entity\AccountCountPrice;
use PaymentBundle\Form\Type\PriceType;

class PriceController extends Controller
{
    public function viewAction()
    {
        $prices = $this->getDoctrine()->getRepository('PaymentBundle:AccountCountPrice')->findAll();

        return $this->render('PaymentBundle:Price:view.html.twig', array('prices' => $prices));
    }

    public function createAction(Request $request)
    {
        $form = $this->createForm(PriceType::class, new AccountCountPrice());
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST')
        {
            if ($form->isValid())
            {

                $data = $form->getData();

                $em = $this->getDoctrine()->getManager();
                $em->persist($data);
                $em->flush();

                return $this->redirect($this->generateUrl('price_view'));
            }
        }
        return $this->render('PaymentBundle:Price:create.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id, Request $request)
    {
        $price = $this->getDoctrine()->getRepository('PaymentBundle:AccountCountPrice')->find($id);

        $form = $this->createForm(PriceType::class, $price);
        $form->handleRequest($request);

        if ($request->getMethod() == 'POST')
        {
            if ($form->isValid())
            {
                $data = $form->getData();

                $em = $this->getDoctrine()->getManager();
                $em->persist($data);
                $em->flush();

                return $this->redirect($this->generateUrl('price_view'));
            }
        }
        return $this->render('PaymentBundle:Price:create.html.twig', array('form' => $form->createView(), 'price' => $price));
    }

    public function deleteAction($id)
    {
        $price = $this->getDoctrine()->getRepository('PaymentBundle:AccountCountPrice')->find($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($price);
        $em->flush();

        return $this->redirect($this->generateUrl('price_view'));
    }
}
