<?php

namespace PaymentBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PaymentBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="payment_order")
 * @ORM\Entity(repositoryClass="PaymentBundle\Entity\Repository\OrderRepository")
 */
class Order
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $user_id;

    /**
     * @ORM\Column(name="summ", type="float")
     */
    protected $summ;

    /**
     * @ORM\Column(name="count_profile", type="integer")
     */
    protected $count_profile;

    /**
     * @ORM\Column(name="order_id", type="string")
     */
    protected $order_id;

    /**
     * @ORM\Column(name="subscribe", type="boolean")
     */
    protected $subscribe;

    /**
     * @ORM\Column(name="advertising_and_jingles", type="boolean")
     */
    protected $advertising_and_jingles;

    /**
     * @ORM\Column(name="state", type="boolean")
     */
    protected $state;

    /**
     * @ORM\Column(name="creation", type="datetime")
     */
    protected $creation;

    /**
     * @ORM\Column(name="ended", type="datetime", nullable = true)
     */
    protected $ended;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Users", inversedBy="account_orders")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $users;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set summ
     *
     * @param integer $summ
     *
     * @return Order
     */
    public function setSumm($summ)
    {
        $this->summ = $summ;

        return $this;
    }

    /**
     * Get summ
     *
     * @return integer
     */
    public function getSumm()
    {
        return $this->summ;
    }

    /**
     * Set creation
     *
     * @param \DateTime $creation
     *
     * @return Order
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime
     */
    public function getCreation()
    {
        return $this->creation;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Order
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set users
     *
     * @param \UserBundle\Entity\Users $users
     *
     * @return Order
     */
    public function setUsers(\UserBundle\Entity\Users $users = null)
    {
        $this->users = $users;

        return $this;
    }

    /**
     * Get users
     *
     * @return \UserBundle\Entity\Users
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set subscribe
     *
     * @param integer $subscribe
     *
     * @return Order
     */
    public function setSubscribe($subscribe)
    {
        $this->subscribe = $subscribe;

        return $this;
    }

    /**
     * Get subscribe
     *
     * @return integer
     */
    public function getSubscribe()
    {
        return $this->subscribe;
    }

    /**
     * Set state
     *
     * @param boolean $state
     *
     * @return Order
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return boolean
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set orderId
     *
     * @param integer $orderId
     *
     * @return Order
     */
    public function setOrderId($orderId)
    {
        $this->order_id = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return integer
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * Set ended
     *
     * @param \DateTime $ended
     *
     * @return Order
     */
    public function setEnded($ended)
    {
        $this->ended = $ended;

        return $this;
    }

    /**
     * Get ended
     *
     * @return \DateTime
     */
    public function getEnded()
    {
        return $this->ended;
    }

    /**
     * Set countProfile
     *
     * @param integer $countProfile
     *
     * @return Order
     */
    public function setCountProfile($countProfile)
    {
        $this->count_profile = $countProfile;

        return $this;
    }

    /**
     * Get countProfile
     *
     * @return integer
     */
    public function getCountProfile()
    {
        return $this->count_profile;
    }

    /**
     * Set accountCountPriceId
     *
     * @param integer $accountCountPriceId
     *
     * @return Order
     */
    public function setAccountCountPriceId($accountCountPriceId)
    {
        $this->account_count_price_id = $accountCountPriceId;

        return $this;
    }

    /**
     * Get accountCountPriceId
     *
     * @return integer
     */
    public function getAccountCountPriceId()
    {
        return $this->account_count_price_id;
    }

    /**
     * Set advertisingAndJingles
     *
     * @param boolean $advertisingAndJingles
     *
     * @return Order
     */
    public function setAdvertisingAndJingles($advertisingAndJingles)
    {
        $this->advertising_and_jingles = $advertisingAndJingles;

        return $this;
    }

    /**
     * Get advertisingAndJingles
     *
     * @return boolean
     */
    public function getAdvertisingAndJingles()
    {
        return $this->advertising_and_jingles;
    }
}
