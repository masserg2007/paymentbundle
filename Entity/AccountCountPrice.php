<?php

namespace PaymentBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PaymentBundle\Entity
 *
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="account_count_prices")
 */
class AccountCountPrice
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="count_account", type="integer")
     */
    protected $count_account;

    /**
     * @ORM\Column(name="price", type="integer")
     */
    protected $price;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set countAccount
     *
     * @param integer $countAccount
     *
     * @return AccountCountPrice
     */
    public function setCountAccount($countAccount)
    {
        $this->count_account = $countAccount;

        return $this;
    }

    /**
     * Get countAccount
     *
     * @return integer
     */
    public function getCountAccount()
    {
        return $this->count_account;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return AccountCountPrice
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

}
